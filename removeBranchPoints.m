function bTree = removeBranchPoints(bTree, Points)

l = size(Points,2);

for i = 1:l;

    bTree(Points(1,i)-1,Points(2,i)-1) = 0;
    bTree(Points(1,i),  Points(2,i)  ) = 0;
    bTree(Points(1,i)-1,Points(2,i)+1) = 0;

    bTree(Points(1,i),  Points(2,i)-1) = 0;
    bTree(Points(1,i),  Points(2,i)  ) = 0;
    bTree(Points(1,i),  Points(2,i)+1) = 0;

    bTree(Points(1,i)+1,Points(2,i)-1) = 0;
    bTree(Points(1,i)+1,Points(2,i)  ) = 0;
    bTree(Points(1,i)+1,Points(2,i)+1) = 0;

end
