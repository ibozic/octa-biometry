function [ONH, vasculature] = extractONHvessels(vTree, Points, xc, yc, r)

% 
% xc = 416;
% yc = 114;
% r  = 58;

Dist = distanceONH(Points, xc, yc);

Dist(Dist<r) = 0;

for i = 1:size(Points,2);
    
    if Dist(i) == 0
            
        vTree(Points(1,i)-1,Points(2,i)-1) = 0;
        vTree(Points(1,i),  Points(2,i)  ) = 0;
        vTree(Points(1,i)-1,Points(2,i)+1) = 0;

        vTree(Points(1,i),  Points(2,i)-1) = 0;
        vTree(Points(1,i),  Points(2,i)  ) = 0;
        vTree(Points(1,i),  Points(2,i)+1) = 0;

        vTree(Points(1,i)+1,Points(2,i)-1) = 0;
        vTree(Points(1,i)+1,Points(2,i)  ) = 0;
        vTree(Points(1,i)+1,Points(2,i)+1) = 0;
        
    end
    
end

CC = bwconncomp(vTree,8);
numPixels = cellfun(@numel,CC.PixelIdxList);

W = numPixels;
W(W<=(mean(numPixels)-std(numPixels))) = 0;
% W(W<median(numPixels)) = 0;
ONH = vTree;

for i = 1:length(W)
    if (W(i)~=0)
        ONH(CC.PixelIdxList{i}) = 0;
    end
end

[G, num] = bwlabel(vTree-ONH);



vasculature = zeros(size(vTree,1), size(vTree,2), num);

for i = 1:num
    [r,c] = find(G == i);
    for j = 1:length(r)
        vasculature(r(j),c(j),i) = 1;
    end
end

