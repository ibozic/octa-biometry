function Dist = distanceONH(Points, xc, yc)
l = size(Points,2);
Dist = zeros(1, l);
for i = 1:l;
    
    Dist(i) = ((Points(2,i)-xc)^2+(Points(1,i)-yc)^2)^(1/2);
    
end

