function [p] = labelingVasulature(vTree, kernel, xc, yc, p)
global GEN BxF IDX
% disp('Funkcija Pozvana')
% size(p)
[R3, C3, ~, ~] = branchPoints(vTree, kernel);
bPoints = [R3; C3];
%     

num = size(bPoints,2);

% if num == 1
% %     disp('Usao 1')
%     vTree = removeBranchPoints(vTree, bPoints);
%     tmp2 = bwconncomp(vTree, 8);
%     teTree = bwmorph(vTree,'endpoints');
%     [tr,tc] = find(teTree == 1);
%     tePoints = [tr tc]';
%     teDist = distanceONH(tePoints, xc, yc);
%     members = zeros(1, tmp2.NumObjects);
%     idx = find(teDist == min(teDist));
%     tmpI = 500*(tePoints(2,idx)-1)+tePoints(1,idx);
%     for i = 1:tmp2.NumObjects
%         lia = ismember(tmp2.PixelIdxList{i},tmpI);
%         members(i) = sum(lia);
%     end
%     idxG = find(members == 1);
%     for i = 1:tmp2.NumObjects
%         if i == idxG
%             vTree(tmp2.PixelIdxList{i}) = 2+GEN;
%         else
%             vTree(tmp2.PixelIdxList{i}) = 3+GEN;
%         end
%     end
%     
%     p = p+vTree;

% else
    
    if num == 0
%     disp('Usao 0')

    BxF.branch(IDX).gen = GEN;
    BxF.branch(IDX).len = sum(vTree(:));
    vTree(vTree == 1) = GEN;
    p = p+vTree;
    IDX = IDX+1;


    
else
%     disp('Usao else')
%     GEN = 2;
    bDist = distanceONH(bPoints, xc, yc);
    idx = find(bDist == min(bDist));
    vTree = removeBranchPoints(vTree, bPoints(:,idx));
    tmp2 = bwconncomp(vTree, 8);
    teTree = bwmorph(vTree,'endpoints');
    [tr,tc] = find(teTree == 1);
    tePoints = [tr tc]';
    teDist = distanceONH(tePoints, xc, yc);
    members = zeros(1, tmp2.NumObjects);
    idx = find(teDist == min(teDist));
    tmpI = 500*(tePoints(2,idx)-1)+tePoints(1,idx);
    for i = 1:tmp2.NumObjects
        lia = ismember(tmp2.PixelIdxList{i},tmpI);
        members(i) = sum(lia);
    end
    idxG = find(members == 1);
    
    tmp1 = vTree;
    for i = 1:tmp2.NumObjects
        if i ~= idxG
            vTree(tmp2.PixelIdxList{i}) = 0;
        end
    end
%    size(bPoints)
   [p] =  labelingVasulature(vTree, kernel, xc, yc, p);
       GEN = GEN+1;
%    disp('Prosao I')
   vTree = tmp1;
   vTree(tmp2.PixelIdxList{idxG}) = 0;
   vTree = bwlabel(vTree);
   vTree1 = vTree == 1;
   vTree2 = vTree == 2;
   vTree = double(vTree1);
   [p] = labelingVasulature(vTree, kernel, xc, yc, p);
%    disp('Prosao II')


   vTree = double(vTree2);
   [p] = labelingVasulature(vTree, kernel, xc, yc, p);
%    disp('Prosao III')
%    rr = size(bPoints,2)
       GEN = GEN-size(bPoints,2);
%    GEN
end

            
        
