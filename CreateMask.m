function mask = CreateMask(par)

if nargin <1
    par = 0
end

mask = zeros(3,3,18);
mask(:,:,1) = [1 0 1; 0 1 0; 0 1 0];
mask(:,:,2) = [0 0 1; 1 1 0; 0 0 1];
mask(:,:,3) = [0 1 0; 0 1 0; 1 0 1];
mask(:,:,4) = [1 0 0; 0 1 1; 1 0 0];

mask(:,:,5) = [1 0 1; 0 1 0; 0 0 1];
mask(:,:,6) = [0 0 1; 0 1 0; 1 0 1];
mask(:,:,7) = [1 0 0; 0 1 0; 1 0 1];
mask(:,:,8) = [1 0 1; 0 1 0; 1 0 0];

mask(:,:,9)  = [0 1 0; 1 1 1; 0 0 0];
mask(:,:,10) = [0 1 0; 0 1 1; 0 1 0];
mask(:,:,11) = [0 0 0; 1 1 1; 0 1 0];
mask(:,:,12) = [0 1 0; 1 1 0; 0 1 0];

mask(:,:,13) = [0 1 0; 1 1 0; 0 0 1];
mask(:,:,14) = [0 1 0; 0 1 1; 1 0 0];
mask(:,:,15) = [1 0 0; 0 1 1; 0 1 0];
mask(:,:,16) = [0 0 1; 1 1 0; 0 1 0];

mask(:,:,17) = [1 0 1; 0 1 0; 1 0 1];
mask(:,:,18) = [0 1 0; 1 1 1; 0 1 0];

if par == 1

    figure
    subplot(5,4,1); imagesc(mask(:,:,1)); colormap gray; axis off; axis image; 
    title('Mask 1');
    subplot(5,4,2); imagesc(mask(:,:,2)); colormap gray; axis off; axis image; 
    title('Mask 2');
    subplot(5,4,3); imagesc(mask(:,:,3)); colormap gray; axis off; axis image; 
    title('Mask 3');
    subplot(5,4,4); imagesc(mask(:,:,4)); colormap gray; axis off; axis image; 
    title('Mask 4');
    subplot(5,4,5); imagesc(mask(:,:,5)); colormap gray; axis off; axis image; 
    title('Mask 5');
    subplot(5,4,6); imagesc(mask(:,:,6)); colormap gray; axis off; axis image; 
    title('Mask 6');
    subplot(5,4,7); imagesc(mask(:,:,7)); colormap gray; axis off; axis image; 
    title('Mask 7');
    subplot(5,4,8); imagesc(mask(:,:,8)); colormap gray; axis off; axis image; 
    title('Mask 8');
    subplot(5,4,9); imagesc(mask(:,:,9)); colormap gray; axis off; axis image; 
    title('Mask 9');
    subplot(5,4,10); imagesc(mask(:,:,10)); colormap gray; axis off; axis image; 
    title('Mask 10');
    subplot(5,4,11); imagesc(mask(:,:,11)); colormap gray; axis off; axis image; 
    title('Mask 11');
    subplot(5,4,12); imagesc(mask(:,:,12)); colormap gray; axis off; axis image; 
    title('Mask 12');
    subplot(5,4,13); imagesc(mask(:,:,13)); colormap gray; axis off; axis image; 
    title('Mask 13');
    subplot(5,4,14); imagesc(mask(:,:,14)); colormap gray; axis off; axis image; 
    title('Mask 14');
    subplot(5,4,15); imagesc(mask(:,:,15)); colormap gray; axis off; axis image; 
    title('Mask 15');
    subplot(5,4,16); imagesc(mask(:,:,16)); colormap gray; axis off; axis image; 
    title('Mask 16');
    subplot(5,4,18); imagesc(mask(:,:,17)); colormap gray; axis off; axis image; 
    title('Mask 17');
    subplot(5,4,19); imagesc(mask(:,:,18)); colormap gray; axis off; axis image; 
    title('Mask 18');

end